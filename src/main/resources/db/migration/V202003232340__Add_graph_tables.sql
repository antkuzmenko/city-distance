create table node
(
    city varchar(64) not null,
    constraint node_pk primary key (city)
);

create table edge
(
    id       bigint(10)  not null auto_increment,
    city1    varchar(64) not null,
    city2    varchar(64) not null,
    distance int(10)     not null,
    constraint edge_pk primary key (id),
    constraint edge_city1_node_city_fk foreign key (city1) references node (city),
    constraint edge_city2_node_city_fk foreign key (city2) references node (city),
    constraint edge_no_circle check (city1 <> city2)
);
