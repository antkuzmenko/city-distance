insert into city_distance_db.node (city)
values ('Minsk'),
       ('Vitebsk'),
       ('Grodno'),
       ('Gomel'),
       ('Mogilev'),
       ('Brest');

insert into city_distance_db.edge (city1, city2, distance)
values ('Minsk', 'Vitebsk', 275),
       ('Vitebsk', 'Minsk', 275),
       ('Grodno', 'Minsk', 280),
       ('Minsk', 'Grodno', 280),
       ('Gomel', 'Grodno', 592),
       ('Grodno', 'Gomel', 592),
       ('Mogilev', 'Brest', 529),
       ('Brest', 'Mogilev', 529),
       ('Mogilev', 'Minsk', 198),
       ('Minsk', 'Mogilev', 198);

