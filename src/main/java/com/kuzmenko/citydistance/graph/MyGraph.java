package com.kuzmenko.citydistance.graph;

import lombok.NoArgsConstructor;

import java.util.*;

@NoArgsConstructor
public class MyGraph<V, E> {

    // Adjacency table for storing Vertices and edges
    private Map<V, List<E>> adjacencyVertices = new HashMap<>();

    public void addEmptyVertex(V v) {
        adjacencyVertices.putIfAbsent(v, new ArrayList<>());
    }

    public void addEdge(V v, E e) {
        adjacencyVertices.get(v).add(e);
    }

    public void addVertexWithEdges(V v, List<E> eList) {
        adjacencyVertices.putIfAbsent(v, eList);
    }

    public boolean containsVertex(V v) {
        return adjacencyVertices.containsKey(v);
    }

    public List<E> getEdgesByVertex(V v) {
        return adjacencyVertices.get(v);
    }

}
