package com.kuzmenko.citydistance.controller;

import com.kuzmenko.citydistance.dto.RouteDto;
import com.kuzmenko.citydistance.model.City;
import com.kuzmenko.citydistance.service.CityDistanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class CityDistanceController {

    private CityDistanceService cityDistanceService;

    @Autowired
    public CityDistanceController(CityDistanceService cityDistanceService) {
        this.cityDistanceService = cityDistanceService;
    }

    @GetMapping("/set-distance")
    public void setDistanceBetweenCities(@RequestParam(value = "firstCity") String firstCity,
                                         @RequestParam(value = "secondCity") String secondCity,
                                         @RequestParam(value = "distance") Integer distance) {
        cityDistanceService.setCitiesAndDistance(new City(firstCity), new City(secondCity), distance);
    }

    @GetMapping("/get-route")
    public List<RouteDto> getRoutesBetweenCities(@RequestParam(value = "firstCity") String firstCity,
                                                 @RequestParam(value = "secondCity") String secondCity) {
        return cityDistanceService.getAllRoutesBetweenCities(new City(firstCity), new City(secondCity));
    }
}
