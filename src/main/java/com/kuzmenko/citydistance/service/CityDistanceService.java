package com.kuzmenko.citydistance.service;

import com.kuzmenko.citydistance.dto.RouteDto;
import com.kuzmenko.citydistance.model.City;
import com.kuzmenko.citydistance.model.CityDistanceGraph;
import com.kuzmenko.citydistance.model.Distance;
import com.kuzmenko.citydistance.repository.CityRepository;
import com.kuzmenko.citydistance.repository.DistanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CityDistanceService {

    private DistanceRepository distanceRepository;
    private CityRepository cityRepository;
    private CityDistanceGraph cityDistanceGraph;

    @Autowired
    public CityDistanceService(DistanceRepository distanceRepository, CityRepository cityRepository, CityDistanceGraph cityDistanceGraph) {
        this.distanceRepository = distanceRepository;
        this.cityRepository = cityRepository;
        this.cityDistanceGraph = cityDistanceGraph;
        initializeDistanceGraphFromDb();
    }

    @Transactional
    public void setCitiesAndDistance(City firstCity, City secondCity, Integer distance) {

        if (!distanceRepository.existsByFirstCityAndSecondCity(firstCity, secondCity)) {
            cityRepository.save(firstCity);
            cityRepository.save(secondCity);
            distanceRepository.save(new Distance(firstCity, secondCity, distance));
            distanceRepository.save(new Distance(secondCity, firstCity, distance));
            initializeDistanceGraphFromDb();
        }

    }

    public List<RouteDto> getAllRoutesBetweenCities(City firstCity, City secondCity) {

        return null;
    }

    private void initializeDistanceGraphFromDb() {
        cityRepository.findAll().forEach(
                el -> cityDistanceGraph.addVertexWithEdges(el, distanceRepository.findAllByFirstCity(el))
        );

    }
}
