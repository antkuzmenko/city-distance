package com.kuzmenko.citydistance.model;

import lombok.*;

import javax.persistence.*;

@Data
@EqualsAndHashCode
@NoArgsConstructor

@Entity(name = "edge")
public class Distance {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @OneToOne
    @JoinColumn(name = "city1")
    private City firstCity;
    @NonNull
    @OneToOne
    @JoinColumn(name = "city2")
    private City secondCity;
    @NonNull
    @Column(name = "distance")
    private Integer distance;

    public Distance(City firstCity, City secondCity, Integer distance) {
        this.firstCity = firstCity;
        this.secondCity = secondCity;
        this.distance = distance;
    }
}
