package com.kuzmenko.citydistance.model;

import com.kuzmenko.citydistance.graph.MyGraph;
import org.springframework.stereotype.Component;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Stack;

@Component
// Graph data structure
public class CityDistanceGraph extends MyGraph<City, Distance> {

//TODO
    Set<City> depthFirstTraversal(City startPoint,City endPoint) {
        Set<City> visited = new LinkedHashSet<>();
        Stack<City> stack = new Stack<>();
        stack.push(startPoint);
        while (!stack.isEmpty()) {
            City vertex = stack.pop();
            if (vertex.equals(endPoint)) {

            }
            if (!visited.contains(vertex)) {
                visited.add(vertex);
                for (Distance edge : getEdgesByVertex(vertex)) {
                    stack.push(edge.getSecondCity());
                }
            }
        }
        return visited;
    }

//TODO
    Set<City> search(City startPoint,City endPoint) {
        Set<City> visited = new LinkedHashSet<>();
        Stack<City> stack = new Stack<>();
        stack.push(startPoint);

        City check = new City();


        while (!check.equals(endPoint)) {
            City vertex = stack.pop();
            if (!visited.contains(vertex)) {
                visited.add(vertex);
                for (Distance edge : getEdgesByVertex(startPoint)) {
                    stack.push(edge.getSecondCity());
                }
            }
        }
        return visited;
    }
}
