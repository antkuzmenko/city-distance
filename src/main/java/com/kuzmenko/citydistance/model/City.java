package com.kuzmenko.citydistance.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor

@Entity(name = "node")
public class City {

    @Id
    @NonNull
    @Column(name = "city")
    private String cityName;
}
