package com.kuzmenko.citydistance.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode
@AllArgsConstructor

public class RouteDto {

    private List<RouteStepDto> routeScheme;
    private int totalDistance;

    public RouteDto(List<RouteStepDto> routeScheme) {
        this.routeScheme = routeScheme;
        routeScheme.stream().map(e -> totalDistance + e.getDistance());
    }


}
