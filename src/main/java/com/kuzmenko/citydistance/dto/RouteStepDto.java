package com.kuzmenko.citydistance.dto;

import com.kuzmenko.citydistance.model.City;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@AllArgsConstructor

//Elementary step in all way
public class RouteStepDto {
    private City startPoint;
    private City endPoint;
    private Integer distance;
}
