package com.kuzmenko.citydistance.repository;

import com.kuzmenko.citydistance.model.City;
import com.kuzmenko.citydistance.model.Distance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DistanceRepository extends JpaRepository<Distance, Long> {

    List<Distance> findAllByFirstCity(City city);

    Boolean existsByFirstCityAndSecondCity(City firstCity, City secondCity);
}
