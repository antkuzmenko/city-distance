package com.kuzmenko.citydistance.repository;

import com.kuzmenko.citydistance.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {


}
